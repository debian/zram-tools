zram-tools
==========

Scripts for managing zram device, currently only for zramswap,
but more tools could be implemented in the future, such as managing
/tmp on zram.

zramswap start
--------------

Sets up zram devices and initializes swap.

zramswap stop
-------------

Removes all current zram swap space and device.

zramswap status
---------------

Shows information on data stored in zram swap.

/etc/default/zramswap
---------------------

Configuration file for zramswap.
